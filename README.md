# 🎵barfax-web🎵

The webapp for barfax

---

## Getting Started

- Install (optional)(Node managed with NPM)

```bash
    nvm install v11.11.0
```

- Install (NPM Modules)

```bash
    npm install
```

- Development

Check package.json for full list of scripts to run

```bash
    npm run build # Builds app assets (compress images, robots, favicon, etc)

    npm run clean # Remove output from build

    npm run start # Run dev server with browser0sync

    npm run storybook # Run a storybook (UI test harness) files located in lib/stories

    npm run watch # Rebuild assets on change

```

Scripts should be simple in nature and can be combined into larger scripts using various tools like `concurrently` (see the `build` script for example in `package.json`)

Functionality that becomes too cumbersome to express via scripts in `package.json` should be placed in the `scripts/` directory (see `scripts/cpy-imgs.js` for example)

---

## Development

The T.S.S.S.S.C method

**T**ests **S**tories **S**tores **S**ervices **S**creens **C**omponents


### Architecture

1. An "app" is composed of multiple Screens which are composed of functional HTML Components.

2. Each Store represents the state for some domain (up to you)
    - A Store for each screen and/or a single store for the entire app state

3. Each Service encapsulates some side-effect that the store needs
    - auth service (e.g. [Auth0](https://auth0.com/))
    - storage service (e.g. [DropBox](https://www.dropbox.com/), [GoogleDrive](https://www.google.com/drive/), HTML5 Local Storage)
    - crash Logs & analytics (e.g. [Sentry](https://www.sentry.io))
    - etc

4. Each Test covers a service or store
5. Each Story covers screen or component

---

## Testing

Test are split into two categories behavior (`BDD`) and results (`RDD`) which allow a more flexible test development. BDD is most reflected in the UI where the conern is the presentation of the data in all states (pending, error, success, fetching, etc) rather than the validity, for this we use [storybook](https://storybook.js.org/) (see stories in `lib/stories` and config in `.storybook`). RDD is more traditional testing where the output is compared against some expected value, for this we use [tape](https://github.com/substack/tape) as a harness (see `lib/tests`).

*Note: Usually each file will only need to be tested either RDD or BDD style but rarely both so if you find yourself testing results in a UI component maybe consider moving that functionality to a separate module and injecting it where needed (see `lib/utils.js or lib/liduen.js`)*

---

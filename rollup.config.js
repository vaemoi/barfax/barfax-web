import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';
import json from 'rollup-plugin-json';
import { terser } from 'rollup-plugin-terser';

export default {
  external: [
    `dompurify`,
    `localforage`,
    `mobx`,
    `mobx-react-lite`,
    `mobx-utils`,
    `navi`,
    `prop-types`,
    `react`,
    `react-dom`,
    `react-modal`,
    `react-navi`,
    `react-outside-click-handler`
  ],
  input: `lib/bootstrap.js`,
  output: {
    file: `public/js/barfax.js`,
    format: `iife`,
    sourcemap: true,
    globals: {
      'dompurify': `DOMPurify`,
      'localforage': `localforage`,
      'mobx': `mobx`,
      'mobx-react-lite': `mobxReactLite`,
      'mobx-utils': `mobxUtils`,
      'navi': `Navi`,
      'prop-types': `PropTypes`,
      'react': `React`,
      'react-dom': `ReactDOM`,
      'react-modal': `ReactModal`,
      'react-navi': `ReactNavi`,
      'react-outside-click-handler': `OutsideClickHandler`
    },
  },
  plugins: [
    resolve({browser: true}),
    json(),
    babel({runtimeHelpers: true}),
    commonjs(),
    terser(),
  ],
}

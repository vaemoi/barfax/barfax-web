import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { text } from '@storybook/addon-knobs';

import { CenterDecorator } from './helpers/center-decorator.js';
import { mockDisplay } from './helpers/mocks.js';

import { Dingus } from '../components/dingus.js';

storiesOf(`Components/Dingus`, module)
  .addDecorator(CenterDecorator)
  .add(`normal`, () => {
    const
      mockItem = {
        blurb: {
          rich: text(`Blurb Rich`)
        },
        blurbFlow: text(`Blurb Flow`, ``),
        updateBlurb: action(`Blurb updated`)
      },
      mockPlaceholder = text(`Placeholder`, `Drop some heat`);

    return (
      <Dingus
        item={mockItem}
        display={mockDisplay()}
        placeHolder={mockPlaceholder}
      />
    );
  });

import React from 'react';
import { storiesOf } from '@storybook/react';

import { CenterDecorator } from './helpers/center-decorator.js';
import { SpriteSheetDecorator } from './helpers/spritesheet-decorator.js';
import { mockDisplay, mockPreview } from './helpers/mocks.js';

import { Repertoire } from '../components/repertoire.js';

storiesOf(`Components/Repertoire`, module)
  .addDecorator(CenterDecorator)
  .addDecorator(SpriteSheetDecorator)
  .add(`normal`, () => {
    return (
      <Repertoire items={mockPreview(5)} display={mockDisplay()}/>
    );
  });

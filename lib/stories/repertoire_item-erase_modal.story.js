import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, text } from '@storybook/addon-knobs';

import { CenterDecorator } from './helpers/center-decorator.js';

import { RepertoireItemEraseModal } from '../components/repertoire_item-erase_modal.js';

storiesOf(`Modals/Item Erase`, module)
  .addDecorator(CenterDecorator)
  .add(`normal`, () => {
    const
      uxi = {
        modal: {
          erase: boolean(`Toggle Modal`, false),
          eraseID: 0,
          itemTitle: text(`item title`, `Lorem Ipsum`)
        }
      },
      anchor = `root`;

    return (
      <RepertoireItemEraseModal uxi={uxi} anchor={anchor}/>
    );
  });

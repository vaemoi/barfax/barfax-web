import React from 'react';
import { storiesOf } from '@storybook/react';

import { CenterDecorator } from './helpers/center-decorator.js';
import { SpriteSheetDecorator } from './helpers/spritesheet-decorator.js';
import { mockDisplay, mockPreview } from './helpers/mocks.js';

import { RepertoireItem } from '../components/repertoire_item.js';
import { RepertoireItemNew } from '../components/repertoire_item-new.js';

storiesOf(`Components/Item`, module)
  .addDecorator(CenterDecorator)
  .add(`new`, () => {
    return (
      <RepertoireItemNew />
    );
  })
  .add(`single`, () => {
    return (
      <RepertoireItem item={mockPreview(1)} display={mockDisplay()}/>
    );
  }, { decorators: [SpriteSheetDecorator]});

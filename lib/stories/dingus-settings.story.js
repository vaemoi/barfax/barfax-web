import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { boolean, select, text } from '@storybook/addon-knobs';

import { VIBES } from '../constants.js';
import { CenterDecorator } from './helpers/center-decorator.js';
import { SpriteSheetDecorator } from './helpers/spritesheet-decorator.js';
import { mockFlavors } from './helpers/mocks.js';

import { DingusSettings } from '../components/dingus-settings.js';

storiesOf(`Components/Dingus Settings`, module)
  .addDecorator(CenterDecorator)
  .addDecorator(SpriteSheetDecorator)
  .add(`normal`, () => {
    const
      mockItem = {
        config: {
          flavor: select(`Flavor`, mockFlavors, mockFlavors.chedda),
          title: text(`Item Title`, ``),
          vibe: select(`Vibe`, VIBES, VIBES.jive)
        },
        updateConfig: action(`Flavor Dropdown`)
      },

      mockUxi = {
        dropdown: {
          flavor: boolean(`Flavor Dropdown`, false)
        },
        toggleFlavorDropdown: action(`Flavor Dropdown Toggled!`)
      };

    return (
      <article className={`settings`}>
        <DingusSettings item={mockItem} uxi={mockUxi}/>
      </article>
    );
  });

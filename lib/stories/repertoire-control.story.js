import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { CenterDecorator } from './helpers/center-decorator.js';
import { SpriteSheetDecorator } from './helpers/spritesheet-decorator.js';
import { mockDisplay } from './helpers/mocks.js';

import { RepertoireControl } from '../components/repertoire-control.js';

storiesOf(`Controls/Repertoire`, module)
  .addDecorator(CenterDecorator)
  .addDecorator(SpriteSheetDecorator)
  .add(`regular`, () => {
    return (
      <RepertoireControl display={mockDisplay()} toggleFn={action(`Toggle clicked`)} />
    );
  });

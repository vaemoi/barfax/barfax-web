import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { boolean, select, text } from '@storybook/addon-knobs';

import { SETTINGS, VIBES } from '../constants.js';
import { CenterColumnDecorator } from './helpers/center_column-decorator.js';
import { SpriteSheetDecorator } from './helpers/spritesheet-decorator.js';
import { mockFlavors } from './helpers/mocks.js';

import { Settings } from '../screens/settings.js';

storiesOf(`Screens/Settings`, module)
  .addDecorator(CenterColumnDecorator)
  .addDecorator(SpriteSheetDecorator)
  .add(`from repertoire`, () => {
    const mockInfo = () => {
      return {
        item: null,
        settings: {
          defaultFlavor: {title: `Default Flavor`, value: select(`Default Flavor`, mockFlavors, mockFlavors.chedda)}
        },
        uxi: {
          dropdown: {
            defaultFlavor: boolean(`Default Flavor Dropdown`, false)
          },
          toggleDefaultFlavorDropdown: action(`Default Flavor Dropdown Toggled!`)
        },
        updateSetting: action(`Setting Updated`)
      };
    };

    return (
      <Settings info={mockInfo} />
    );
  })
  .add(`from pad`, () => {
    const mockInfo = () => {
      return {
        item: {
          config: {
            flavor: select(`Flavor`, mockFlavors, mockFlavors.chedda),
            title: text(`Item Title`, ``),
            vibe: select(`Vibe`, VIBES, VIBES.jive)
          },
          updateConfig: action(`Flavor Dropdown`)
        },
        settings: SETTINGS,
        uxi: {
          dropdown: {
            defaultFlavor: boolean(`Default Flavor Dropdown`, false),
            flavor: boolean(`Flavor Dropdown`, false)
          },
          toggleDefaultFlavorDropdown: action(`Default Flavor Dropdown Toggled!`),
          toggleFlavorDropdown: action(`Flavor Dropdown Toggled!`)
        },
        updateSetting: action(`Setting Updated`)
      };
    };

    return (
      <Settings info={mockInfo} />
    );
  });

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { boolean, select, text } from '@storybook/addon-knobs';

import { MODES } from '../constants.js';
import { CenterColumnDecorator } from './helpers/center_column-decorator.js';
import { SpriteSheetDecorator } from './helpers/spritesheet-decorator.js';
import { mockFlavors, mockStats } from './helpers/mocks.js';

import { Pad } from '../screens/pad.js';

storiesOf(`Screens/Pad`, module)
  .addDecorator(CenterColumnDecorator)
  .addDecorator(SpriteSheetDecorator)
  .add(`regular`, () => {
    const mockInfo = () => {
      return {
        uxi: {
          display: {
            dingusMode: select(`Dingus Mode`, MODES, MODES.compose)
          },
          modeIcon: select(`Mode Icon`, {pencil: `pencil`, wipe: `wipe`, flow: `flow`}, `pencil`),
          openModeDropdown: action(`Mode Dropdown Opened`),
          switchModes: action(`Mode Switched`),
          dropdown: { mode: boolean(`Mode Dropdown Toggle`, false) }
        },
        placeHolder: `Drop some heat`,
        item: {
          blurbFlow: `All these rappers sayin' they spitting hard raps, before I buy that shit show me the barfax <br/> I got a tongue like a sharp axe <br/> I got a ton of rhymes flyer than anything launching off tarmacs`,
          displayTitle: text(`Title`, `Rung up`),
          statsDisplay: mockStats(),
          updateBlurb: action(`Blurb updated`),
          blurb: {
            rich: `All these rappers sayin' they spitting hard raps, before I buy that shit show me the barfax // I got a tongue like a sharp axe // I got a ton of rhymes flyer than anything launching off tarmacs`
          },
          config: { flavor: select(`Flavor`, mockFlavors, mockFlavors.chedda) }
        }
      };
    };

    return (
      <Pad info={mockInfo} />
    );
  });

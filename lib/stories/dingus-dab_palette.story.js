import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { CenterDecorator } from './helpers/center-decorator.js';
import { FlavorDecorator } from './helpers/flavor-decorator.js';

import { DingusDabPalette } from '../components/dingus-dab_palette.js';

storiesOf(`Components/Dab Palette`, module)
  .addDecorator(CenterDecorator)
  .addDecorator(FlavorDecorator)
  .add(`normal`, () => {
    return (
      <DingusDabPalette dabbaDo={action(`Dab button clicked`)}/>
    );
  });

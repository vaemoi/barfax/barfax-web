import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { select } from '@storybook/addon-knobs';

import { SpriteSheetDecorator } from './helpers/spritesheet-decorator.js';
import { mockFlavors } from './helpers/mocks.js';

import { Navbar } from '../components/navbar.js';

// Add a null
mockFlavors.none = null;

storiesOf(`Components/Navbar`, module)
  .addDecorator(SpriteSheetDecorator)
  .add(`on Home`, () => {
    const homeNavigation = {
      getCurrentValue: () => {
        return {
          title: `Home`,
          url: {params: {}},
          data: {}
        };
      },

      goBack: action(`back-button-clicked`)
    };

    return (
      <header className={`app-header`}>
        <Navbar navigation={homeNavigation}/>
      </header>
    );
  })
  .add(`on Pad`, () => {
    const padNavigation = {
      getCurrentValue: () => {
        return {
          title: `Pad`,
          url: {params: { id: 1 }},
          data: {flavor: select(`Flavor`, mockFlavors, mockFlavors.none)}
        };
      },

      goBack: action(`back-button-clicked`)
    };

    return (
      <header className={`app-header`}>
        <Navbar navigation={padNavigation}/>
      </header>
    );
  })
  .add(`on Settings`, () => {
    const settingsNavigation = {
      getCurrentValue: () => {
        return {
          title: `Settings`,
          url: {params: {}},
          data: {flavor: select(`Flavor`, mockFlavors, mockFlavors.none)}
        };
      },

      goBack: action(`back-button-clicked`)
    };

    return (
      <header className={`app-header`}>
        <Navbar navigation={settingsNavigation}/>
      </header>
    );
  });

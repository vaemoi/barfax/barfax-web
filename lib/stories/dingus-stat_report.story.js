import React from 'react';
import { storiesOf } from '@storybook/react';

import { CenterDecorator } from './helpers/center-decorator.js';
import { mockStats } from './helpers/mocks.js';

import { DingusStatReport } from '../components/dingus-stat_report.js';

storiesOf(`Components/Stat Report`, module)
  .addDecorator(CenterDecorator)
  .add(`empty`, () => {
    return (
      <DingusStatReport stats={mockStats()}/>
    );
  });

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { boolean, number, select, text } from '@storybook/addon-knobs';

import { LAYOUTS } from '../constants.js';
import { CenterColumnDecorator } from './helpers/center_column-decorator.js';
import { SpriteSheetDecorator } from './helpers/spritesheet-decorator.js';
import { mockFlavors } from './helpers/mocks.js';

import { Home } from '../screens/home.js';

storiesOf(`Screens/Home`, module)
  .addDecorator(CenterColumnDecorator)
  .addDecorator(SpriteSheetDecorator)
  .add(`regular`, () => {
    const
      vibeObj = {
        jive: `jive`,
        square: `square`
      },
      mockPreviews = () => {
        const
          createTime = Date.now(),
          previews = [];

        for (let index = 5; index > -1; index--) {
          previews.push({
            id: index,
            flavor: select(`Flavor ${index}`, mockFlavors, mockFlavors.chedda),
            title: text(`Barfax title ${index}`, `Weight Scale`),
            text: text(`Barfax preview ${index}`, `All these rappers saying they spitting hard facts // before I buy that shit...`),
            bars: number(`Bar Count ${index}`, 0),
            barsDisplay: text(`Bar Count display ${index}`, `0`),
            barsSize: select(`Bars Size ${index}`, {normal: `normal`, large: `large`, xlarge: `xlarge`}, `normal`),
            words: number(`Word Count ${index}`, 0),
            wordsDisplay: text(`Word Count Display ${index}`, `0`),
            syllables: number(`Syllable Count ${index}`, 0),
            syllablesDisplay: text(`Syllable Count Display ${index}`, `0`),
            createTime: number(`Create Time ${index}`, createTime),
            createTimeDisplay: text(`Create Time Display ${index}`, new Date(createTime).toDateString()),
            modTime: number(`Mod Time ${index}`, createTime),
            modTimeDisplay: text(`Mod Time Display ${index}`, new Date(createTime).toDateString())
          });
        }

        return previews;
      },
      mockInfo = () => {
        return {
          items: mockPreviews(),
          uxi: {
            anchor: `root`,
            display: {
              repertoireLayout: select(`Repertoire Layout`, LAYOUTS, LAYOUTS.grid)
            },
            dropdown: {
              flavor: boolean(`Toggle Flavor Dropdown`, false)
            },
            modal: {
              edit: boolean(`Toggle Edit/Create Modal`, false),
              erase: boolean(`Toggle Erase Modal`, false),
              eraseID: 0,
              itemTitle: text(`Item Title`, `Lorem Ipsum`),
              itemVibe: select(`Item Vibess`, vibeObj, vibeObj.jive),
              itemFlavor: select(`Item Flavor`, mockFlavors, mockFlavors.chedda)
            },
            openFlavorDropdown: action(`Flavor Dropdown Opened`),
            closeFlavorDropdown: action(`Flavor Dropdown Closed`),
            toggleLayout: action(`Layout Toggled`),
            updateEditModal: action(`Modal Updated`)
          }
        };
      };

    return (
      <Home info={mockInfo}/>
    );
  });

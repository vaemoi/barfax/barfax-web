import React from 'react';
import PropTypes from 'prop-types';
import { select } from '@storybook/addon-knobs';

import { mockFlavors } from './mocks.js';

export const FlavorDecorator = (storyFn) => {
  return (
    <div className={`center-container ${select(`Page Flavor`, mockFlavors, mockFlavors.blush)}`}>
      {storyFn()}
    </div>
  );
};

FlavorDecorator.displayName = `FlavorDecorator`;
FlavorDecorator.propTypes = {
  storyFn: PropTypes.func.isRequired
};

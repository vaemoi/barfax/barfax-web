import React from 'react';
import PropTypes from 'prop-types';

export const CenterColumnDecorator = (storyFn) => {
  return (
    <div className={`center-container-col`}>
      <div className={`app-header`}></div>
      {storyFn()}
    </div>
  );
};

CenterColumnDecorator.displayName = `CenterColumnDecorator`;
CenterColumnDecorator.propTypes = {
  storyFn: PropTypes.func.isRequired
};

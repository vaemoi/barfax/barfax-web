import React from 'react';
import PropTypes from 'prop-types';

export const CenterDecorator = (storyFn) => {
  return (
    <div className={`center-container`}>
      {storyFn()}
    </div>
  );
};

CenterDecorator.displayName = `CenterDecorator`;
CenterDecorator.propTypes = {
  storyFn: PropTypes.func.isRequired
};

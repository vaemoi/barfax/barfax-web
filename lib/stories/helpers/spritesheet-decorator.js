import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import spriteSheet from '-!svg-inline-loader!../../../assets/svg/barfax-sprites.svg';

export const SpriteSheetDecorator = (storyFn) => {
  return (
    <Fragment>
      <div style={{display: `none`}} dangerouslySetInnerHTML={{__html: spriteSheet}}/>
      {storyFn()}
    </Fragment>
  );
};

SpriteSheetDecorator.displayName = `SpriteSheetDecorator`;
SpriteSheetDecorator.propTypes = {
  storyFn: PropTypes.func.isRequired
};

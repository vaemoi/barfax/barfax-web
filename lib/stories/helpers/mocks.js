import { number, select, text } from '@storybook/addon-knobs';

import { FLAVORS, LAYOUTS, MODES } from '../../constants.js';

const mockDisplay = () => {
  return {
    dingusMode: select(`Dingus Mode`, MODES, MODES.compose),
    repertoireLayout: select(`Repertoire Layout`, LAYOUTS, LAYOUTS.grid)
  };
};

const mockFlavors = FLAVORS.reduce((final, current) => {
  final[current] = current;

  return final;
}, {});

const mockPreview = (count=1) => {
  const
    createTime = Date.now(),
    previews = [];

  for (let index = count-1; index > -1; index--) {
    previews.push({
      id: index,
      flavor: select(`Flavor ${index}`, mockFlavors, mockFlavors.chedda),
      title: text(`Barfax title ${index}`, `Weight Scale`),
      text: text(`Barfax preview ${index}`, `All these rappers saying they spitting hard facts // before I buy that shit...`),
      bars: number(`bar count`, 0),
      barsDisplay: text(`bar display`, `0`),
      barsSize: select(`Bars Size`, {normal: `normal`, large: `large`, xlarge: `xlarge`}, `normal`),
      words: number(`Barfax word count`, 0),
      wordsDisplay: text(`words display`, `0`),
      syllables: number(`Barfax syllable count`, 0),
      syllablesDisplay: text(`syllables display`, `0`),
      createTime: number(`Barfax create time`, createTime),
      createTimeDisplay: text(`Barfax create time`, new Date(createTime).toDateString()),
      modTime: number(`Barfx mod time`, createTime),
      modTimeDisplay: text(`Barfx mod time`, new Date(createTime).toDateString())
    });
  }

  return count === 1 ? previews.pop() : previews;
};

const mockStats = () => {
  return {
    bars: number(`bar count`, 0),
    barsDisplay: text(`bar count display`, `0`),
    words: number(`word count`, 0),
    wordsDisplay: text(`word count display`, `0`),
    syllables: number(`syllable count`, 0),
    syllablesDisplay: text(`syllable count display`, `0`),
  };
};

export {
  mockDisplay,
  mockFlavors,
  mockPreview,
  mockStats
};

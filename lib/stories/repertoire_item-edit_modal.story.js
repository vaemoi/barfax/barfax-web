import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, select, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import { VIBES } from '../constants.js';
import { SpriteSheetDecorator } from './helpers/spritesheet-decorator.js';
import { mockFlavors } from './helpers/mocks.js';

import { RepertoireItemEditModal } from '../components/repertoire_item-edit_modal.js';

const anchor = `root`;

storiesOf(`Modals/Item Edit`, module)
  .addDecorator(SpriteSheetDecorator)
  .add(`edit`, () => {
    const uxi = {
      dropdown: {
        flavor: boolean(`Toggle Flavor Dropdown`, false)
      },
      modal: {
        edit: boolean(`Toggle Modal`, true),
        itemTitle: text(`Item Title`, `Lorem Ipsum`),
        itemVibe: select(`Item Vibe`, VIBES, VIBES.jive),
        itemFlavor: select(`Item Flavor`, mockFlavors, mockFlavors.chedda)
      },
      toggleFlavorDropdown: action(`Flavor Dropdown Opened`),
      updateEditModal: action(`Modal Updated`)
    };

    return (
      <RepertoireItemEditModal uxi={uxi} anchor={anchor}/>
    );
  })
  .add(`create`, () => {
    const
      vibeObj = {
        jive: `jive`,
        square: `square`
      },
      uxi = {
        dropdown: {
          flavor: boolean(`Toggle Flavor Dropdown`, false)
        },
        modal: {
          create: boolean(`Toggle Modal`, true),
          itemTitle: text(`Item Title`, null),
          itemVibe: select(`Item Vibe`, vibeObj, vibeObj.jive),
          itemFlavor: select(`Item Flavor`, mockFlavors, mockFlavors.space)
        },
        openFlavorDropdown: action(`Flavor Dropdown Opened`),
        closeFlavorDropdown: action(`Flavor Dropdown Closed`),
        updateEditModal: action(`Modal Updated`)
      };

    return (
      <RepertoireItemEditModal uxi={uxi} anchor={anchor}/>
    );
  });

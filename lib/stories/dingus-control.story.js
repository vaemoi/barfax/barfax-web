import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, select } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import { MODES } from '../constants.js';
import { CenterDecorator } from './helpers/center-decorator.js';
import { SpriteSheetDecorator } from './helpers/spritesheet-decorator.js';

import { DingusControl } from '../components/dingus-control.js';

storiesOf(`Controls/Dingus`, module)
  .addDecorator(CenterDecorator)
  .addDecorator(SpriteSheetDecorator)
  .add(`regular`, () => {
    const
      mockUXI = {
        display: {
          dingusMode: select(`Dingus Mode`, MODES, MODES.compose)
        },
        dropdown: {
          mode: boolean(`Dropdown open`, false)
        },
        switchModes: action(`Mode Switched`),
        toggleModeDropdown: action(`Dropdown opened`),
        modeIcon: select(`Mode Icon`, {pencil: `pencil`, wipe: `wipe`, flow: `flow`}, `pencil`)
      };

    return (
      <DingusControl uxi={mockUXI} />
    );
  });

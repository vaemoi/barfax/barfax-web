import DOMPurify from 'dompurify';

const getNewID = (currentIDs) => {
  let id = Math.random() * Math.floor(10000);

  if (typeof currentIDs === `object` && Array.isArray(currentIDs)) {
    while (currentIDs.includes(id)) {
      id = Math.random() * Math.floor(10000);
    }
  }

  return id;
};

const getRandomValueFrom = (values) => {
  let randomIndex = null;

  if (typeof values === `string` || (typeof values === `object` && Array.isArray(values)))
    randomIndex = Math.floor(Math.random * Math.floor(values.length));

  return values[randomIndex];
};

const pruneHTML = (html) => {
  // TODO: Add options here
  return DOMPurify.sanitize(html);
};

const textFromHTML = (domString) => {
  let result = ``;

  if (typeof domString === `string`) {
    const html = new DOMParser().parseFromString(domString);

    result = html.body.firstChild.innerText;
  }

  return result;
};

// TODO: Don't highlight leading/trailing whitespace and don't highlight the '//'
const highlightSelection = (dab) => {
  const
    highlightedSelection = window.getSelection(),
    highlightedRange = highlightedSelection.getRangeAt(0),
    container = highlightedRange.commonAncestorContainer.parentElement;

  if (container.className !== dab) {
    if (container.innerText === highlightedSelection.toString() && container.tagName !== `ARTICLE`) {
      container.className = dab;
    } else {
      const
        highlightedText = highlightedRange.extractContents(),
        wrapper = document.createElement(`span`);

      wrapper.appendChild(highlightedText);
      wrapper.className = dab;
      highlightedRange.insertNode(wrapper);
    }
  }

  highlightedSelection.collapseToEnd();
};

const capitalize = (word) => {
  return `${word[0].toUpperCase()}${word.substr(1)}`;
};

export {
  capitalize,
  getNewID,
  getRandomValueFrom,
  highlightSelection,
  pruneHTML,
  textFromHTML
};

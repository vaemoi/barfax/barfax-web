export const DABS = [`clear`, `pink`, `red`, `orange`, `brown`, `yellow`, `green`, `marine`, `brey`];
export const FLAVORS = [`fig`, `space`, `chedda`, `blush`, `mint`, `breeze`];

export const MODE_NAMES = [`compose`, `emphasize`, `flow`];
export const MODES = {
  compose: `compose`,
  emphasize: `emphasize`,
  flow: `flow`
};

export const LAYOUTS = {
  grid: 0,
  list: 1
};

export const SETTINGS = {
  defaultFlavor: {title: `Default Flavor`, value: FLAVORS[2]}
};

export const VIBES = {
  jive: `jive`,
  square: `square`
};

export const CONFIGS = {
  flavor: `flavor`,
  title: `title`,
  vibe: `vibe`
};

export const PAGES = {
  home: 0,
  pad: 1,
  settings: 2,
  error: 3
};

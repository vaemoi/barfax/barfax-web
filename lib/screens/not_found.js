import React from 'react';
import PropTypes from 'prop-types';

export const NotFound = () => {
  const componentName = `not-found`;

  return (
    <div className={`${componentName}`}>
      <h1 className={`${componentName}-banner`}>Page Not Found!</h1>
    </div>
  );
};

NotFound.displayName = `NotFound`;
NotFound.propTypes = {
  injectStore: PropTypes.func.isRequired
};

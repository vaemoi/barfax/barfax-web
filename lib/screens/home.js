import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react-lite';

import { Repertoire } from '../components/repertoire.js';
import { RepertoireControl } from '../components/repertoire-control.js';
import { RepertoireItemEditModal } from '../components/repertoire_item-edit_modal.js';
import { RepertoireItemEraseModal } from '../components/repertoire_item-erase_modal.js';

export const Home = observer(({info}) => {
  const
    componentName = `home`,
    {items, uxi} = info();

  return (
    <article className={`${componentName}`}>
      <Repertoire items={items} display={uxi.display} />
      <footer className={`${componentName}-footer`}>
        <RepertoireControl display={uxi.display} toggleFn={uxi.toggleLayout} />
        <RepertoireItemEditModal uxi={uxi} anchor={uxi.anchor}/>
        <RepertoireItemEraseModal uxi={uxi} anchor={uxi.anchor}/>
      </footer>
    </article>
  );
});

Home.displayName = `Home`;
Home.propTypes = {
  info: PropTypes.func.isRequired
};

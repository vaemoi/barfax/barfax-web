import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react-lite';

import { MODES } from '../constants.js';

import { Dingus } from '../components/dingus.js';
import { DingusControl } from '../components/dingus-control.js';
import { DingusDabPalette } from '../components/dingus-dab_palette.js';
import { DingusStatReport } from '../components/dingus-stat_report.js';

export const Pad = observer(({info}) => {
  const
    componentName = `pad`,
    {item, placeHolder, uxi} = info();

  return (
    <article className={`${componentName} ${item.config.flavor} ${uxi.display.dingusMode}`}>
      {uxi.display.dingusMode === MODES[`emphasize`] && <DingusDabPalette dabbado={uxi.applyDab} />}
      <h4 className={`${componentName}-title`}>{item.displayTitle}</h4>
      <Dingus
        item={item}
        display={uxi.display}
        placeHolder={placeHolder} />

      <footer className={`${componentName}-footer`}>
        <DingusStatReport stats={item.statsDisplay} />
        <DingusControl uxi={uxi} />
      </footer>
    </article>
  );
});

Pad.displayName = `PadScreen`;
Pad.propTypes = {
  info: PropTypes.func.isRequired
};

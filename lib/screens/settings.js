import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react-lite';
import OutsideClickHandler from 'react-outside-click-handler';

import { FLAVORS } from '../constants.js';

import { DingusSettings } from '../components/dingus-settings';

export const Settings = observer(({info}) => {
  const
    componentName = `settings`,
    { item, settings, uxi, updateSetting } = info();

  const itemSettings = (
    <Fragment>
      <hr />
      <DingusSettings item={item} uxi={uxi}/>
    </Fragment>
  );

  return (
    <article className={`${componentName} ${item ? item.config.flavor : ``}`}>
      <section className={`app-${componentName}`}>
        <h3>{`App Settings`}</h3>
        <div className={`option-row`}>
          <h3>{settings.defaultFlavor.title}</h3>
          <OutsideClickHandler onOutsideClick={uxi.toggleDefaultFlavorDropdown}>
            <div className={`flavor-input-select ${uxi.dropdown.defaultFlavor ? `open` : ``}`} name={`defaultFlavor`}>
              <div className={`option`} onClick={uxi.toggleDefaultFlavorDropdown}>
                <span className={`${settings.defaultFlavor.value} dot`}></span>
                <span>{`${settings.defaultFlavor.value[0].toUpperCase()}${settings.defaultFlavor.value.substr(1)}`}</span>
                <svg className="icon"><use xlinkHref={`#chevron-down`} /></svg>
              </div>
              <div className={`option-list ${uxi.dropdown.defaultFlavor ? `` : `hidden`}`}>
                {
                  FLAVORS.map((flavor, index) => {
                    const
                      isSelected = settings.defaultFlavor.value === flavor ? `selected` : ``,
                      selectFlavor = () => {
                        updateSetting(`defaultFlavor`, flavor);
                        uxi.toggleDefaultFlavorDropdown();
                      };

                    return (
                      <div
                        key={index}
                        className={`option ${isSelected}`}
                        onClick={selectFlavor}
                      >
                        <span className={`${flavor} dot`}></span>
                        <span>{`${flavor[0].toUpperCase()}${flavor.substr(1)}`}</span>
                      </div>
                    );
                  })
                }
              </div>
            </div>
          </OutsideClickHandler>
        </div>
      </section>
      {item && itemSettings}
    </article>
  );
});

Settings.displayName = `SettingsScreen`;
Settings.propTypes = {
  info: PropTypes.func.isRequired
};

import React, { Fragment } from 'react';
import { NotFoundBoundary, useNavigation } from 'react-navi';
import propTypes from 'prop-types';
import { observer } from 'mobx-react-lite';

import { Navbar } from './components/navbar.js';
import { NotFound } from './screens/not_found.js';

export const App = observer(({children}) => {
  const componentName = `app`;

  return (
    <Fragment>
      <header className={`${componentName}-header`}>
        <Navbar navigation={useNavigation()}/>
      </header>
      <span id={`gutter`}/>
      <main className={`${componentName}-main`}>
        <NotFoundBoundary render={NotFound}>
          {children}
        </NotFoundBoundary>
      </main>
    </Fragment>
  );
});

App.displayName = `App`;
App.propTypes = {
  children: propTypes.object.isRequired
};

import { action, computed, observable, } from 'mobx';

import { DABS, FLAVORS, LAYOUTS, MODE_NAMES, MODES, PAGES, VIBES } from '../constants.js';
import { highlightSelection } from '../utils.js';

export const UXIStoreFactory = (_flavor, _anchor) => {
  const
    anchor = _anchor,
    navigation = observable({
      currentPage: PAGES[`home`]
    }),
    display = observable({
      navbarFlavor: null,
      dingusMode: MODES[`compose`],
      repertoireLayout: LAYOUTS[`grid`]
    }),
    dropdown = observable({
      defaultFlavor: _flavor,
      flavor: false,
      mode: false
    }),
    selected = observable({
      dab: -1,
      barfax: -1,
    }),
    modal = observable({
      edit: false,
      erase: false,
      itemTitle: ``,
      itemFlavor: _flavor,
      itemVibe: VIBES[`jive`],
      editID: null,
      eraseID: null
    });

  // Computed-s
  const modeIcon = computed(() => {
    let icon;

    switch(display.dingusMode) {
    case MODES[`compose`]:
      icon = `pencil`; break;
    case MODES[`emphasize`]:
      icon = `wipe`; break;
    case MODES[`flow`]:
      icon = `flow`; break;
    }

    return icon;
  });

  // Actions
  const
    handleNavigation = action((page, barfaxID) => {
      switch(page) {
      case PAGES[`home`]:
        navigation.currentPage = PAGES[`home`];
        selected.barfax = -1;
        display.dingusMode = MODES[`compose`];
        resetEditModal();
        resetEraseModal();
        break;
      case PAGES[`pad`]:
        navigation.currentPage = PAGES[`pad`];
        selected.barfax = barfaxID;
        resetEditModal();
        resetEraseModal();
        break;
      case PAGES[`settings`]:
        navigation.currentPage = PAGES[`settings`];
        resetEditModal();
        resetEraseModal();
        break;
      default:
        navigation.currentPage = PAGES[`error`];
        resetEditModal();
        resetEraseModal();
      }
    }),

    switchModes = action((mode) => {
      if (MODE_NAMES.includes(mode) && display.dingusMode !== mode) {
        display.dingusMode = mode;
      }

      dropdown.mode = false;
    }),

    switchDabs = action((dab) => {
      if (DABS.includes(dab) && selected.dab !== dab) {
        selected.dab = dab;
      }
    }),

    toggleLayout = action(() => {
      if (display.repertoireLayout === LAYOUTS[`list`]) {
        display.repertoireLayout = LAYOUTS[`grid`];
      } else {
        display.repertoireLayout = LAYOUTS[`list`];
      }
    }),

    toggleModeDropdown = action(() => {
      if (dropdown.mode) {
        dropdown.mode = false;
      } else  {
        dropdown.mode = true;
      }
    }),

    toggleFlavorDropdown = action(() => {
      if (dropdown.flavor) {
        dropdown.flavor = false;
      } else {
        dropdown.flavor = true;
      }
    }),

    toggleDefaultFlavorDropdown = action(() => {
      if (dropdown.defaultFlavor) {
        dropdown.defaultFlavor = false;
      } else {
        dropdown.defaultFlavor = true;
      }
    }),

    updateEditModal = action((key, value) => {
      if (Object.keys(modal).includes(key) && modal.key !== value) {
        modal.key = value;
      }

      toggleFlavorDropdown();
    }),

    openEditModal = action((item = null) => {
      if (!modal.edit) {
        modal.edit = true;

        if (item) {
          modal.editID = item.id;
          modal.itemTitle = item.title;
          modal.itemFlavor = item.flavor;
        } else {
          modal.itemFlavor = _flavor;
        }
      }
    }),

    openEraseModal = action((item) => {
      if (!modal.erase) {
        modal.erase = true;
        modal.eraseID = item.id;
        modal.itemTitle = item.title;
      }
    }),

    resetEditModal = action(() => {
      modal.edit = false;
      modal.editID = null;
      modal.itemTitle = ``;
      modal.itemFlavor = _flavor;
      modal.itemVibe = VIBES[`jive`];

      toggleFlavorDropdown();
    }),

    resetEraseModal = action(() => {
      modal.erase = false;
      modal.eraseID = null;
      modal.itemTitle = ``;
    }),

    setNavbarFlavor = action((flavor) => {
      if (FLAVORS.includes(flavor) && display.dingusFlavor !== flavor) {
        display.dingusFlavor = flavor;
      }
    }),

    applyDab = action((dab) => {
      if (DABS.includes(dab) && display.currentPage === PAGES[`pad`]) {
        selected.dab = DABS.indexOf(dab);

        highlightSelection(dab);
      }
    });

  return {
    anchor,
    display,
    handleNavigation,
    modal,
    modeIcon,
    navigation,
    openEditModal,
    openEraseModal,
    toggleModeDropdown,
    toggleFlavorDropdown,
    toggleDefaultFlavorDropdown,
    resetEraseModal,
    resetEditModal,
    selected,
    setNavbarFlavor,
    switchDabs,
    switchModes,
    toggleLayout,
    updateEditModal,
    applyDab
  };
};

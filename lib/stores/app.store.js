import { observable, action, autorun, toJS } from 'mobx';
import { computedFn } from 'mobx-utils';

import { getNewID, getRandomValueFrom } from '../utils.js';
import { SETTINGS } from '../constants.js';
import { BarFaxStoreFactory } from './barfax.store.js';
import { UXIStoreFactory } from './uxi.store.js';

export const AppStoreFactory = (storage, _settings = SETTINGS, _barfaxes = [], anchor) => {
  const
    barfaxes = observable(_barfaxes),
    settings = observable(_settings),
    uxi = UXIStoreFactory(_settings.defaultPadFlavor, anchor),
    placeHolders = [
      `Drop some heat`,
      `Get it started`,
      `Feel the flow`
    ];

  // Computed-s

  const
    homeInfo = computedFn(() => {
      const previews = barfaxes.map((barfax) => { barfax.preview; });

      return {
        items: previews,
        uxi: uxi
      };
    }),

    padInfo = computedFn(() => {
      const padBarFax = barfaxes.find((barfax) => {
        return barfax.id === uxi.selected.barfax;
      });

      uxi.setNavbarFlavor(padBarFax.config.flavor);

      return {
        item: padBarFax,
        placeHolder: getRandomValueFrom(placeHolders),
        uxi: uxi
      };
    }),

    settingsInfo = computedFn(() => {
      const settingsBarFax = uxi.selected.barfax ? barfaxes.find((barfax) => {
        return barfax.id === uxi.selected.barfax;
      }) : null;

      if (settingsBarFax) {
        uxi.setNavbarFlavor(settingsBarFax.config.flavor);
      }

      return {
        item: settingsBarFax,
        settings: Array.from(settings),
        updateSettings: updateSetting,
        uxi: uxi

      };
    });

  // Actions

  const
    createBarFax = action(() => {
      const
        barfaxIDs = barfaxes.map((barfax) => { barfax.id; }),
        newID = getNewID(barfaxIDs),
        newBarFax = BarFaxStoreFactory(
          Date.now(), newID, uxi.modal.itemFlavor, uxi.modal.itemTitle
        );

      barfaxes.unshift(newBarFax);
      storage.saveItem(newID, newBarFax);

      return newID;
    }),

    editBarfax = action(() => {
      const barfaxToEdit = barfaxes.find((barfax) => {
        return barfax.id === uxi.modal.editID;
      });

      barfaxToEdit.updateConfig(`title`, uxi.modal.itemTitle);
      barfaxToEdit.updateConfig(`flavor`, uxi.modal.itemFlavor);

      storage.saveItem(uxi.modal.editID, barfaxToEdit.exportData());
    }),

    eraseBarFax = action(() => {
      const indexToRemove = barfaxes.findIndex((barfax) => {
        return barfax.id === uxi.modal.eraseID;
      });

      if (indexToRemove !== -1) {
        barfaxes.splice(indexToRemove, 1);
        storage.deleteItem(uxi.modal.eraseID);
        uxi.resetEraseModal();
      }
    }),

    saveBarfax = action(() => {
      const barfaxToSave = barfaxes.find((barfax) => {
        return barfax.id === uxi.selected.barfax;
      });

      storage.saveItem(uxi.selected.barfax, barfaxToSave.exportData());
    }),

    updateSetting = action((setting, newValue) => {
      if (Object.keys(settings).includes(setting) && settings[setting] !== newValue) {
        settings[setting] = newValue;

        storage.saveItem(`settings`, toJS(settings));
      }
    });

  // Save opened barfax every 5 seconds while on the pad screen
  autorun(saveBarfax, {
    delay: 5000,
    scheduler: (run) => {
      if (uxi.navigation.currentPage === `Pad` && uxi.selected.barfax !== -1) {
        run();
      }
    }
  });

  return {
    createBarFax,
    editBarfax,
    eraseBarFax,
    homeInfo,
    padInfo,
    settingsInfo,
    uxi
  };
};

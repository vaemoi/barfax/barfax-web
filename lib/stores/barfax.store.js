import { observable, action, computed, toJS } from 'mobx';
import { computedFn } from 'mobx-utils';

import { pruneHTML, textFromHTML } from '../utils.js';
import { countBars, countWords, countSyllables } from '../liduen.js';

export const BarFaxStoreFactory = (timestamp, _id, _flavor, _title, _vibe) => {
  const
    id = _id,

    blurb = observable({
      rich: ``,
      raw: ``
    }),

    config = observable({
      flavor: _flavor,
      title: _title,
      vibe: _vibe
    }),

    meta = observable({
      createTime: timestamp,
      modTime: timestamp
    }),

    stats = observable({
      wordCount: 0,
      barCount: 0,
      syllableCount: 0
    });

  // Computed-s

  const
    titleDisplay = computed(() => { config.title; }),

    preview = computedFn(() => {
      const
        previewText = blurb.raw.substring(Math.min(300, blurb.raw.length)),
        suffix = blurb.raw.length <= 300 ? `` : ` ...`;

      return {
        id: id,
        title: config.title,
        text: `${previewText}${suffix}`,
        flavor: config.flavor,
        words: stats.wordCount,
        syllables: stats.syllableCount,
        bars: stats.barCount,
        barsDisplay: stats.barCount === 999 ? `999+` : stats.barCount.toString(),
        barsSize: stats.barCount > 99 ? stats.barCount > 999 ? `xlarge` : `large` : `normal`,
        createTime: meta.createTime,
        createTimeDisplay: new Date(meta.createTime).toDateString(),
        modTime: meta.modTime,
        modTimeDisplay: new Date(meta.modTime).toDateString()
      };
    }),

    statsDisplay = computedFn(() => {
      return {
        bars: stats.barCount,
        barsDisplay: stats.barCount > 999 ? `999+` : stats.barCount.toString(),
        syllables: stats.syllableCount,
        syllablesDisplay: stats.syllableCount > 999 ? `999+` : stats.syllableCount.toString(),
        words: stats.wordCount,
        wordsDisplay: stats.wordCount > 999 ? `999+` : stats.wordCount.toString()
      };
    }),

    blurbFlow = computed(() => {
      return blurb.rich.replace(/\/\//g, `<br/>`);
    }),

    exportData = computedFn(() => {
      return {
        id: id,
        blurb: toJS(blurb),
        config: toJS(config),
        meta: toJS(meta),
        stats: toJS(stats)
      };
    });

  // Actions

  const
    updateBlurb = action((html) => {
      blurb.rich = pruneHTML(html);
      blurb.raw = textFromHTML(html);

      updateStats();

      meta.modTime = Date.now();
    }),

    // Possible bottleneck here -- investigate async actions
    updateStats = action(() => {
      stats.wordCount = countWords(blurb.raw);
      stats.barCount = countBars(blurb.raw);
      stats.syllableCount = countSyllables(blurb.raw);
    }),

    updateConfig = action((key, newValue) => {
      if (Object.keys(config).includes(key) && config[key] != newValue) {
        config[key] = newValue;
      }
    });

  return {
    id,
    blurb,
    blurbFlow,
    config,
    exportData,
    preview,
    statsDisplay,
    titleDisplay,
    updateBlurb,
    updateConfig
  };
};

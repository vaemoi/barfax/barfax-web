import { mount, redirect, route } from 'navi';
import React from 'react';

import { NotFound } from './screens/not_found.js';
import { Home } from './screens/home.js';
import { Pad } from './screens/pad.js';
import { Settings } from './screens/settings.js';

export const routesFactory = (store) => {
  return mount({
    '/': route((req) => {
      const barfaxID = req.params.id;

      if (!Number.isInteger(barfaxID)) {
        store.uxi.handleNavigation(`Error`);

        return {
          title: `Not Found`,
          view: <NotFound />
        };
      }

      let barfaxItem = null;

      if (req.params.edit) {
        barfaxItem = store.barfaxes.find((barfax) => {
          return barfax.id == barfaxID;
        });

        store.uxi.openEditModal(barfaxItem);
      } else if (req.params.erase) {
        barfaxItem = store.barfaxes.find((barfax) => {
          return barfax.id == barfaxID;
        });

        store.uxi.openEraseModal(barfaxItem);
      } else if (req.params.create) {
        store.uxi.openEditModal();
      }

      store.uxi.handleNavigation(`Home`);

      return {
        title: `Home`,
        view: (
          <Home info={store.homeInfo()} />
        )
      };
    }),

    '/create': route(() => {
      redirect(`/pad?id=${store.createBarfax()}`);
    }),

    '/delete': route(() => {
      store.removeBarfax();
      redirect(`/`);
    }),

    '/edit': route(() => {
      store.editBarfax();
      redirect(`/`);
    }),

    '/pad': route((req) => {
      const padID = req.params.id;

      if (!padID) {
        store.uxi.handleNavigation(`Error`);

        return {
          title: `Not Found`,
          view: <NotFound />
        };
      }

      store.uxi.handleNavigation(`Pad`, padID);

      return {
        title: `Pad`,
        data: {
          flavor: store.uxi.display.navbarFlavor
        },
        view: <Pad info={store.padInfo} />
      };
    }),

    '/settings': route((req) => {
      const padID = req.params.id;

      store.uxi.handleNavigation(`Settings`, padID);

      return {
        title: `Settings`,
        data: {
          flavor: store.uxi.display.navbarFlavor
        },
        view: <Settings info={store.settingsInfo} />
      };
    })
  });
};

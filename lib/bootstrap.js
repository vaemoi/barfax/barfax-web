import React, { Suspense } from 'react';
import { render } from 'react-dom';
import { Router, View } from 'react-navi';

import { HjartaServiceFactory } from './services/hjarta.service.js';
import { AppStoreFactory } from './stores/app.store.js';
import { routesFactory } from './routing.js';
import { App } from './app.js';

// The #id of the element the screens + modals will render within
const appElement = `app-root`;

// Create a service for persisting the app state
const storageService = HjartaServiceFactory();

// Load any previous app state from storage service
const {barfaxes, settings} = storageService.fetchAllItems();

// Create store to read/modify app state
const store = AppStoreFactory(storageService, settings, barfaxes, appElement);

// Create routing for each screen inject needed app state
const routes = routesFactory(store);

// Render app with navigation
render(
  <Router routes={routes}>
    <App>
      <Suspense fallback={null}>
        <View />
      </Suspense>
    </App>
  </Router>,
  document.querySelector(`#${appElement}`)
);

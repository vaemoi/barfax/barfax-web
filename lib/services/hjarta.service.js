import localforage from 'localforage';

export const HjartaServiceFactory = () => {
  const forage = localforage.createInstance({
    name: `barfax`,
    version: 1.0
  });

  async function fetchAllItems() {
    let keys;

    const result = {
      barfaxes: [],
      settings: {}
    };

    try {
      keys = await forage.keys();

      for (let index = keys.length - 1; index >= 0; index--) {
        const key = keys[index];

        if (key === `settings`) {
          result.settings = forage.getItem(key);
        } else if (Number.isInteger(key)) {
          result.barfaxes.push(forage.getItem(key));
        }
      }

      if (result.barfaxes.length === 0) {
        result.barfaxes = undefined;
      }

      if (result.settings.length === 0) {
        result.settings = undefined;
      }

    } catch (error) {
      console.debug(error);
    }

    return result;
  }

  async function deleteItem(id) {
    try {
      await forage.removeItem(id);
    } catch (error) {
      console.debug(error);
    }
  }

  async function saveItem(id, data) {
    try {
      await forage.setItem(id, data);
    } catch (error) {
      console.debug(error);
    }
  }

  /** Should only be called during development! If the app has loaded via
          bootstrap.js all fetching is handled via the stores/ :) so this is just
          if you need to inspect a specific item
  **/
  async function getItem(id) {
    let item;
    try {
      item = await forage.getItem(id);
    } catch (error) {
      console.debug(error);
    }

    return item;
  }

  return {
    fetchAllItems,
    deleteItem,
    saveItem,
    getItem
  };
};

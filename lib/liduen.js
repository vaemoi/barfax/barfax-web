const isVowel = (letter) => {
  let result = 0;

  if (typeof letter === `string`) {
    result = [`a`, `e`, `i`, `o`, `u`].includes(letter);
  }

  return result;
};

// TODO (maybe): Memoize this
const vowelCount = (word) => {
  let count = 0;

  if (typeof word === `string`) {
    const wordArr = word.split(``);

    for (let x = 0; x < wordArr.length; x++) {
      const letter = wordArr[x].toLowerCase();

      let nextLetter = wordArr[x+1];

      if (nextLetter) {
        nextLetter = nextLetter.toLowerCase();
      }

      if (isVowel(letter) && letter !== nextLetter && !isVowel(nextLetter)) {
        count += 1;
      }
    }

    const
      last = word[word.length - 1].toLowerCase(),
      nextToLast = word[word.length - 2].toLowerCase();

    // Word ending with silent 'y'
    if (last === `y` && !isVowel(nextToLast)) {
      count += 1;
    }

    // Word ending with silent 'e'
    if (word.length > 2 && last === `e` && !isVowel(nextToLast)) {
      count -= 1;
    }

    // Word ending in 'es'
    if (word.length > 2 && nextToLast === `e` && last === `s`) {
      count -= 1;
    }
  }

  return count;
};

const countBars = (sentence) => {
  let result = 0;

  if (typeof sentence === `string`) {
    result = sentence.split(`//`).length;
  }

  return result;
};

// TODO (maybe): Lookup 'real' words in a phonetic dictionary and do raw calculation for rest
const countSyllables = (sentence) => {
  let count = 0;

  if (typeof sentence === `string`) {
    const sentenceArr = sentence.split(` `);

    for (let x = 0; x < sentenceArr.length; x++) {
      count += vowelCount(sentenceArr[x]);
    }
  }

  return count;
};

const countWords = (sentence) => {
  let result = 0;

  if (typeof sentence === `string`) {
    result = sentence.split(` `).length;
  }

  return result;
};

export {
  countBars,
  countSyllables,
  countWords
};
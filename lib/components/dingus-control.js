import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react-lite';

import { MODE_NAMES } from '../constants.js';

export const DingusControl = observer(({uxi}) => {
  const componentName = `dingus-control`;

  let view;

  if (uxi.dropdown.mode) {
    view = (
      <ol>
        {
          MODE_NAMES.map((modeName, index) => {
            const
              isSelected = uxi.display.dingusMode === modeName,

              modeClick = () => {
                uxi.switchModes(modeName);
              };

            return (
              <li key={index} className={`${isSelected}`} onClick={modeClick}>
                <svg>
                  <use xlinkHref={`#${uxi.modeIcon}`}/>
                </svg>
              </li>
            );
          })
        }
      </ol>
    );
  } else {
    view = (
      <button onClick={uxi.toggleModeDropdown}>
        <svg>
          <use xlinkHref={`#${uxi.modeIcon}`}/>
        </svg>
      </button>
    );
  }

  return (
    <div className={componentName}>
      {view}
    </div>
  );
});

DingusControl.displayName = `DingusControl`;
DingusControl.propTypes = {
  uxi: PropTypes.object.isRequired
};

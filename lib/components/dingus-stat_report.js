import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react-lite';

import { DingusStatItem } from './dingus-stat_item.js';

export const DingusStatReport = observer(({stats}) => {
  const componentName = `dingus-stat-report`;

  return (
    <ol className={`${componentName}`}>
      <li>
        <h3>{`words:`}<DingusStatItem value={stats.words} displayValue={stats.wordsDisplay}/></h3>
      </li>
      <li>
        <h3>{`bars:`}<DingusStatItem value={stats.bars} displayValue={stats.barsDisplay}/></h3>
      </li>
      <li>
        <h3>{`syllables:`}<DingusStatItem value={stats.syllables} displayValue={stats.syllablesDisplay}/></h3>
      </li>
    </ol>
  );
});

DingusStatReport.displayName = `DingusStatReport`;
DingusStatReport.propTypes = {
  stats: PropTypes.object.isRequired
};

import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react-lite';

export const TitleInput = observer(({item, uxi}) => {
  let placeholderValue = `(max 70 characters)`;

  if (item) {
    placeholderValue = item.config.title;
  } else if (uxi.modal.itemTitle) {
    placeholderValue = uxi.modal.itemTitle;
  }

  const handleUpdate = (evt) => {
    if (item) {
      item.updateConfig(evt.target.name, evt.target.value);
    } else {
      uxi.updateEditModal(evt.target.name, evt.target.value);
    }
  };

  return (
    <input
      name={item ? `title` : `inputTitle`}
      type={`text`}
      maxLength={70}
      value={item ? item.config.title : uxi.modal.itemTitle}
      placeholder={placeholderValue}
      onChange={handleUpdate}/>
  );
});

TitleInput.displayName =`TitleInput`;
TitleInput.propTypes = {
  item: PropTypes.object,
  uxi: PropTypes.object.isRequired
};

import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react-lite';

import { MODES } from '../constants.js';

export const Dingus = observer(({item, placeHolder, display}) => {
  const
    componentName = `dingus`,

    handleInput = (evnt) => {
      item.updateBlurb(evnt.target.innerHTML);
    },

    handleKeyPress = (evnt) => {
      if (display.dingusMode !== MODES[`compose`]) {
        evnt.preventDefault();
      }
    },

    disableEvent = (evnt) => {
      evnt.preventDefault();
    };

  return (
    <article
      className={componentName}
      contentEditable={display.dingusMode === MODES[`compose`]}
      data-placeholder={placeHolder}
      onInput={handleInput}
      onBlur={handleInput}
      onKeyDown={handleKeyPress}
      onDragOver={disableEvent}
      onPaste={disableEvent}
      onDrop={disableEvent}
      dangerouslySetInnerHTML={{__html: display.dingusMode === MODES[`flow`] ? item.blurbFlow : item.blurb.rich}}
    />
  );
});

Dingus.displayName = `Dingus`;
Dingus.propTypes = {
  item: PropTypes.object.isRequired,
  display: PropTypes.object,
  placeHolder: PropTypes.string.isRequired
};

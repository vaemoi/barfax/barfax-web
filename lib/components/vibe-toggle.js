import React from 'react';
import PropTypes from 'prop-types';

import { VIBES } from '../constants.js';
import { capitalize } from '../utils.js';

export const VibeToggle = ({item, uxi}) => {
  const
    itemVibe = item ? item.config.vibe : uxi.modal.itemVibe,

    handleUpdate = (evt) => {
      if (item) {
        item.updateConfig(evt.target.name, evt.target.value);
      } else {
        uxi.updateEditModal(evt.target.name, evt.target.value);
      }
    };

  return (
    <div className={`vibe-input-row`}>
      {
        Object.keys(VIBES).map((vibe, index) => {
          return (
            <label key={index} className={itemVibe == vibe ? `selected` : ``}>
              <input
                type={`radio`}
                name={item ? `vibe` : `itemVibe`}
                value={vibe}
                checked={itemVibe == vibe}
                onChange={handleUpdate}/>
              <span>{capitalize(vibe)}</span>
            </label>
          );
        })
      }
    </div>
  );
};

VibeToggle.displayName = `VibeToggle`;
VibeToggle.propTypes = {
  item: PropTypes.object,
  uxi: PropTypes.object.isRequired
};

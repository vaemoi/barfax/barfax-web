import React from 'react';
import ReactModal from 'react-modal';
import PropTypes from 'prop-types';
import { Link } from 'react-navi';
import { observer } from 'mobx-react-lite';

import { ConfigInputs } from './config_inputs';

export const RepertoireItemEditModal = observer(({uxi, anchor}) => {
  const
    componentName = `repertoire-item-edit-modal`,
    submitURI = uxi.modal.create ? `/create` : `/edit`,

    // Set modal overlay class names and app root
    contentClassNames = {
      afterOpen: `${componentName}--after-open`,
      base: `${componentName} ${uxi.modal.itemFlavor}`,
      beforeClose: `${componentName}--before-close`
    },
    overlayClassNames = {
      afterOpen: `${componentName}-overlay--after-open`,
      base: `${componentName}-overlay`,
      beforeClose: `${componentName}-overlay--before-close`
    };

  ReactModal.setAppElement(`#${anchor}`);

  return (
    <ReactModal
      className={contentClassNames}
      closeTimeoutMS={350}
      isOpen={uxi.modal.edit || uxi.modal.create}
      overlayClassName={overlayClassNames}
      portalClassName={`${componentName}-portal`}>

      <ConfigInputs uxi={uxi}/>
      <Link className={`${componentName}-submit`}tabIndex={1} rel={`alternate`} href={submitURI}>
        <button type={`submit`}>{`Done`}</button>
      </Link>
    </ReactModal>
  );
});

RepertoireItemEditModal.displayName = `RepertoireItemEditModal`;
RepertoireItemEditModal.propTypes = {
  uxi: PropTypes.object.isRequired,
  anchor: PropTypes.string.isRequired
};

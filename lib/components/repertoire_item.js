import React from 'react';
import { Link } from 'react-navi';
import { observer } from 'mobx-react-lite';
import PropTypes from 'prop-types';

import { LAYOUTS } from '../constants.js';

export const RepertoireItem = observer(({item, display}) => {
  const
    componentName = `repertoire-item`,
    containerType = display.repertoireLayout == LAYOUTS[`list`] ? `list`: `grid`;

  return (
    <div className={`${componentName}-${containerType}-container`}>
      <div className={`${item.flavor} ${componentName}`}>
        <div className={`${componentName}-actions`}>
          <Link
            tabIndex={0}
            rel={`alternate`}
            href={`/?edit=true&id=${item.id}`}>

            <svg className={`icon small`}><use xlinkHref={`#modify`}></use></svg>
          </Link>
          <Link
            tabIndex={0}
            rel={`alternate`}
            href={`/?erase=true&id=${item.id}`}>

            <svg className={`icon`}><use xlinkHref={`#close`}></use></svg>
          </Link>
        </div>
        <Link
          title={item.title}
          tabIndex={0}
          className={`${componentName}-body`}
          rel={`next`}
          href={`/pad?id=${item.id}`}>

          {display.repertoireLayout === LAYOUTS.grid &&
            <h2 className={`${componentName}-title`}>{item.title}</h2>
          }
          <p className={`${componentName}-preview`}>
            {item.text}
            <span className={`fade-overflow`}></span>
          </p>
          <data className={`${componentName}-bars ${item.barsSize}`} value={item.bars}>
            {item.barsDisplay}
          </data>
        </Link>
      </div>
      {display.repertoireLayout === LAYOUTS.list &&
        <div className={`${componentName}-info`}>
          <h3><b>{`Title:`}</b><data >{`${item.title}`}</data></h3>
          <h3><b>{`Words:`}</b><data value={item.words}>{`${item.wordsDisplay}`}</data></h3>
          <h3><b>{`Syllables:`}</b><data value={item.syllables}>{`${item.syllablesDisplay}`}</data></h3>
          <h3><b>{`Bars:`}</b><data value={item.bars}>{`${item.barsDisplay}`}</data></h3>
          <h3><b>{`Created At:`}</b><time dateTime={item.createTime}>{`${item.createTimeDisplay}`}</time></h3>
          <h3><b>{`Modified At:`}</b><time dateTime={item.createTime}>{`${item.modTimeDisplay}`}</time></h3>
        </div>
      }
    </div>
  );
});

RepertoireItem.displayName = `RepertoireItem`;
RepertoireItem.propTypes = {
  item: PropTypes.object.isRequired,
  display: PropTypes.object.isRequired
};

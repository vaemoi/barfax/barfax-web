import React from 'react';
import OutsideClickHandler from 'react-outside-click-handler';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react-lite';

import { FLAVORS } from '../constants.js';
import { capitalize } from '../utils.js';

export const FlavorDropdown = observer(({item, uxi}) => {
  const itemFlavor = item ? item.config.flavor : uxi.modal.itemFlavor;

  return (
    <OutsideClickHandler onOutsideClick={uxi.toggleFlavorDropdown}>
      <div className={`flavor-input-select ${uxi.dropdown.flavor ? `open` : ``}`} name={`itemFlavor`}>
        <div className={`option`} onClick={uxi.toggleFlavorDropdown}>
          <span className={`${itemFlavor} dot`}></span>
          <span>{capitalize(itemFlavor)}</span>
          <svg className="icon"><use xlinkHref={`#chevron-down`} /></svg>
        </div>
        <div className={`option-list ${uxi.dropdown.flavor ? `` : `hidden`}`}>
          {
            FLAVORS.map((flavor, index) => {
              const
                isSelected =  flavor === itemFlavor ? `selected` : ``,
                selectFlavor = () => {
                  if (item) {
                    item.updateConfig(`flavor`, flavor);
                  } else {
                    uxi.updateEditModal(`itemFlavor`, flavor);
                  }

                  uxi.toggleFlavorDropdown();
                };

              return (
                <div
                  key={index}
                  className={`option ${isSelected}`}
                  onClick={selectFlavor}
                >

                  <span className={`${flavor} dot`}/>
                  <span>{capitalize(flavor)}</span>
                </div>
              );
            })
          }
        </div>
      </div>
    </OutsideClickHandler>
  );
});

FlavorDropdown.displayName = `FlavorDropdown`;
FlavorDropdown.propTypes = {
  item: PropTypes.object,
  uxi: PropTypes.object.isRequired
};

import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react-lite';

export const DingusStatItem = observer(({value, displayValue}) => {
  return <data value={value}>{displayValue}</data>;
});

DingusStatItem.displayName = `DingusStatItem`;
DingusStatItem.propTypes = {
  value: PropTypes.number.isRequired,
  displayValue: PropTypes.string.isRequired
};

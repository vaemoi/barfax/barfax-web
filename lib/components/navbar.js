import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-navi';

export const Navbar = ({navigation}) => {
  const componentName = `navbar`;

  const { data, title, url } = navigation.getCurrentValue();

  const backClick = () => {
    navigation.goBack();
  };

  let backButton, menuButton;

  switch(title) {
  case `Pad`:
    backButton = (
      <button className={`back-btn`} onClick={backClick}>
        <svg className="icon">
          <use xlinkHref={`#back-arrow`}></use>
        </svg>
      </button>
    );

    menuButton = (
      <Link className={`menu-btn`} href={`/settings?id=${url.params.id}`}>
        <button>
          <svg className={`icon`}>
            <use xlinkHref={`#menu-ellipse`}></use>
          </svg>
        </button>
      </Link>
    );
    break;
  case `Home`:
    backButton = null;
    menuButton = (
      <Link className={`menu-btn`} href={`/settings`}>
        <button>
          <svg className={`icon`}>
            <use xlinkHref={`#menu-ellipse`}></use>
          </svg>
        </button>
      </Link>
    );
    break;
  default:
    backButton = (
      <button className={`back-btn`} onClick={backClick}>
        <svg className={`icon`}>
          <use xlinkHref={`#back-arrow`}></use>
        </svg>
      </button>
    );
    menuButton = null;
  }

  return (
    <nav className={`${componentName} ${data.flavor ? data.flavor : `default`}`}>
      {backButton}
      <h2 className={`${componentName}-title`}>
        <svg className={`logo`}>
          <use xlinkHref={`#barfax-logo`}></use>
        </svg>
        <b>{`barfax`}</b>
      </h2>
      {menuButton}
    </nav>
  );
};

Navbar.displayName = `Navbar`;
Navbar.propTypes = {
  navigation: PropTypes.object.isRequired
};

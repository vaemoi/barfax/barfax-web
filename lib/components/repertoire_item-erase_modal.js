import React from 'react';
import ReactModal from 'react-modal';
import PropTypes from 'prop-types';
import { Link } from 'react-navi';
import { observer } from 'mobx-react-lite';

export const RepertoireItemEraseModal = observer(({uxi, anchor}) => {
  const
    componentName = `repertoire-item-erase-modal`,

    // Set modal overlay class names and app root
    contentClassNames = {
      afterOpen: `${componentName}--after-open`,
      base: `${componentName}`,
      beforeClose: `${componentName}--before-open`
    },
    overlayClassNames = {
      afterOpen: `${componentName}-overlay--after-open`,
      base: `${componentName}-overlay`,
      beforeClose: `${componentName}-overlay--before-open`
    };

  ReactModal.setAppElement(`#${anchor}`);

  return (
    <ReactModal
      className={contentClassNames}
      closeTimeoutMS={250}
      isOpen={uxi.modal.erase}
      overlayClassName={overlayClassNames}
      portalClassName={`${componentName}-portal`}
    >
      <h3 className={`prompt`}>{`Erase`}&nbsp;<em>{uxi.modal.itemTitle}</em>&nbsp;{`?`}</h3>
      <div className={`actions`}>
        <Link tabIndex={1} rel={`alternate`} href={`/erase`}>
          <button className={`confirm`}>{`Yes`}</button>
        </Link>
        <Link tabIndex={1} rel={`alternate`} href={`/`}>
          <button className={`cancel`}>{`No`}</button>
        </Link>
      </div>
    </ReactModal>
  );
});

RepertoireItemEraseModal.displayName = `RepertoireItemEraseModal`;
RepertoireItemEraseModal.propTypes = {
  uxi: PropTypes.object.isRequired,
  anchor: PropTypes.string.isRequired
};

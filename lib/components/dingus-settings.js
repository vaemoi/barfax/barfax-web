import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react-lite';

import { ConfigInputs } from './config_inputs.js';

export const DingusSettings = observer(({item, uxi}) => {
  const componentName = `dingus-settings`;

  return (
    <section className={componentName}>
      <h3>{`Item Settings`}</h3>
      <ConfigInputs item={item} uxi={uxi}/>
    </section>
  );
});

DingusSettings.displayName = `DingusSettings`;
DingusSettings.propTypes = {
  item: PropTypes.object,
  uxi: PropTypes.object.isRequired
};

import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react-lite';

import { LAYOUTS } from '../constants.js';

export const RepertoireControl = observer(({display, toggleFn}) => {
  const
    componentName = `repertoire-control`,
    layoutIcon = display.repertoireLayout == LAYOUTS[`grid`] ? `list` : `grid`;

  return (
    <button className={componentName} onClick={toggleFn}>
      <svg>
        <use xlinkHref={`#${layoutIcon}`}></use>
      </svg>
    </button>
  );
});

RepertoireControl.displayName = `RepertoireControl`;
RepertoireControl.propTypes = {
  display: PropTypes.object.isRequired,
  toggleFn: PropTypes.func.isRequired
};

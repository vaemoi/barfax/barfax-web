import React from 'react';
import { NavLink } from 'react-navi';

export const RepertoireItemNew = () => {
  const componentName = `repertoire-item`;

  return (
    <NavLink
      title={`Create new barfax`}
      className={`${componentName} new`}
      tabIndex={0}
      rel={`next`}
      href={`/?create=true`}>

      <div id={`create`}>
        <div className={`cross`}></div>
        <div className={`crossm`}></div>
      </div>
    </NavLink>
  );
};

RepertoireItemNew.displayName = `RepertoireItemNew`;

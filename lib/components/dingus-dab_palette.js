import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react-lite';

import { DABS } from '../constants.js';

export const DingusDabPalette = observer(({dabbaDo}) => {
  const componentName = `dingus-dab-palette`;

  return (
    <ol className={componentName}>
      {
        DABS.map((dab, index) => {
          const hilite = () => {
            dabbaDo(dab);
          };

          return (
            <li className={`${componentName}-item`} key={index}>
              <button className={dab} onClick={hilite}></button>
            </li>
          );
        })
      }
    </ol>
  );
});

DingusDabPalette.displayName = `DabPalette`;
DingusDabPalette.propTypes = {
  dabbaDo: PropTypes.func.isRequired
};

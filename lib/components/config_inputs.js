import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react-lite';

import { CONFIGS } from '../constants.js';

import { FlavorDropdown } from './flavor-dropdown.js';
import { TitleInput } from './title-input.js';
import { VibeToggle } from './vibe-toggle.js';

export const ConfigInputs = observer(({item, uxi}) => {
  return (
    <Fragment>
      <div className={`option-row ${item ? `text` : ``}`}>
        <h3>{`${CONFIGS.title}:`}</h3>
        <TitleInput item={item} uxi={uxi}/>
      </div>
      <div className={`option-row`}>
        <h3>{`${CONFIGS.vibe}:`}</h3>
        <VibeToggle item={item} uxi={uxi}/>
      </div>
      <div className={`option-row`}>
        <h3>{`${CONFIGS.flavor}:`}</h3>
        <FlavorDropdown item={item} uxi={uxi}/>
      </div>
    </Fragment>
  );
});

ConfigInputs.displayName =`ConfigInputs`;
ConfigInputs.propTypes = {
  item: PropTypes.object,
  uxi: PropTypes.object.isRequired
};

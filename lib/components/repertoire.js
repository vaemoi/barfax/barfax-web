import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react-lite';

import { LAYOUTS } from '../constants.js';

import { RepertoireItem } from '../components/repertoire_item.js';
import { RepertoireItemNew } from '../components/repertoire_item-new.js';

export const Repertoire = observer(({items, display}) => {
  const
    componentName = `repertoire`,
    layoutDisplay = display.repertoireLayout == LAYOUTS[`grid`] ? `grid` : `list`;

  return (
    <ul className={`${componentName} ${layoutDisplay}`}>
      <li><RepertoireItemNew /></li>
      {items.map((item) => {
        return (
          <li key={item.id}><RepertoireItem item={item} display={display} /></li>
        );
      })}
    </ul>
  );
});

Repertoire.displayName = `Repertoire`;
Repertoire.propTypes = {
  items: PropTypes.array.isRequired,
  display: PropTypes.object.isRequired
};

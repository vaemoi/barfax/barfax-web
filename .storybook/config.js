import { addParameters, addDecorator, configure } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { create, themes } from '@storybook/theming';
import '@storybook/addon-console';

// Create dark theme

const barfaxTheme = create({
  base: 'dark',
  brandTitle: `Barfax`
});

// Set global decorators

addParameters({
  options: {
    theme: barfaxTheme
  },
  viewport: {
    defaultViewport: `responsive`
  }
});

addDecorator(withKnobs);

// Load app styles

import '../styles/critical.styl';
import '../styles/main.styl';

function loadStories() {
  require(`../lib/stories/dingus-control.story.js`);
  require(`../lib/stories/dingus-dab_palette.story.js`);
  require(`../lib/stories/dingus-settings.story.js`);
  require(`../lib/stories/dingus-stat_report.story.js`);
  require(`../lib/stories/dingus.story.js`);
  require(`../lib/stories/home.story.js`);
  require(`../lib/stories/navbar.story.js`);
  require(`../lib/stories/pad.story.js`);
  require(`../lib/stories/repertoire.story.js`);
  require(`../lib/stories/repertoire-control.story.js`);
  require(`../lib/stories/repertoire_item.story.js`);
  require(`../lib/stories/repertoire_item-edit_modal.story.js`);
  require(`../lib/stories/repertoire_item-erase_modal.story.js`);
  require(`../lib/stories/settings.story.js`);
};

configure(loadStories, module);

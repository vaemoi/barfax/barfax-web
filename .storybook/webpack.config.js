const path = require('path');

module.exports = async({config, mode}) => {
  config.module.rules.push({
    test: /\.styl$/,
    loaders: 'style-loader!css-loader!stylus-loader',
    include: path.resolve(__dirname, '../')
  });

  config.module.rules.push({
    test: /\.svg$/,
    loaders: 'svg-inline-loader',
    include: path.resolve(__dirname, '../')
  });

  return config;
};

const path = require(`path`);

const bsConfig = {
  files: [
    path.join(__dirname, `public/css/critical.css`),
    path.join(__dirname, `public/css/main.css`),
    path.join(__dirname, `public/js/barfax.js`)
  ],
  open: false,
  plugins: [{
    module: `bs-html-injector`,
    options: {
      files: `public/index.html`
    }
  }],
  server: `public`,
  single: true
};

require(`browser-sync`).init(bsConfig);

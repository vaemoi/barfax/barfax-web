const
  isVowel = (letter) => {
    return [`a`, `e`, `i`, `o`, `u`].indexOf(letter) != -1;
  },

  syllableCount = (sentence) => {
    let count = 0;
    
    const sentenceArr = sentence.split(` `);
    
    for (let x = 0; x < sentenceArr.length; x++) {
      count += vowelCount(sentenceArr[x]);
    }

    return count;
  },

  vowelCount = (word, dev=false) => {
    let count = 0;

    const wordArr = word.split(``);

    for (let x = 0; x < wordArr.length; x++) {
      const
        letter = wordArr[x],
        nextLetter = wordArr[x+1];
  
      switch(letter.toLowerCase()) {
      case `a`:
      case `e`:
      case `i`:
      case `o`:
      case `u`:
        if (letter != nextLetter && !isVowel(nextLetter)) {
          count += 1;
        }
      }
    }

    const
      last = word[word.length - 1],
      nextToLast = word[word.length - 2];

    // Word ending with silent 'y'
    if (last == `y` && !isVowel(nextToLast)) {
      count += 1;
    }

    // Word ending with silent 'e'
    if (word.length > 2 && last == `e` && !isVowel(nextToLast)) {
      count -= 1;
    }

    // Word ending in 'es'
    if (word.length > 2 && nextToLast == `e` && last == `s`) {
      count -= 1;
    }

    if (dev) {
      console.log(`#{count} syllables in #{word.join}`);
    }

    return count;
  },

  wordCount = (sentence) => {
    return sentence.split(` `).length;
  },

  barCount = (sentence, gab=false) => {
    let result = 0;

    if (gab) {
      result = sentence.split(`//`).length;
    } else {
      result = sentence.split(`//`).length / 2;
    }

    return result;
  },

  getBarfax = (lines, gab) => {
    console.log(`==============================`);
    console.log(lines);
    console.log(`------------------------------`);
    console.log(`------------------------------`);
    console.log(`Syllables: ${syllableCount(lines)}`);
    console.log(`Words: ${wordCount(lines)}`);
    console.log(`Bars: ${barCount(lines, gab)}`);
    console.log(`------------------------------\n`);
  };

const input = `Down on one knee // accidentally using profanity // I can't even say a prayer right // I just want a pair nikes // I just wanna wear a white tee // and keep my hair tight // I just want a fair fight // I just want a fair life // Is that too much to ask for // tell me is that too much to ask for`;

console.log(`\nBarfax example:\n`);
console.log(`\nNo Gab`);
getBarfax(input);
console.log(`\nGab`);
getBarfax(input, true);

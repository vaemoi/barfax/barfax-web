const
  fs = require(`fs`),
  cheerio = require(`cheerio`),
  path = require(`path`);

let
  svgSpriteSheet = fs.readFileSync(
    path.join(__dirname, `../assets/svg/barfax-sprites.svg`)
  ),
  indexHTML = cheerio.load(fs.readFileSync(
    path.join(__dirname, `../public/index.html`)
  ));

if (Buffer.isBuffer(svgSpriteSheet)) {
  svgSpriteSheet = svgSpriteSheet.toString();
}

if (Buffer.isBuffer(indexHTML)) {
  indexHTML = indexHTML.toString();
}

indexHTML(svgSpriteSheet).insertBefore(`#app-root`)
fs.writeFileSync(path.join(__dirname, `../public/index.html`), indexHTML.html());

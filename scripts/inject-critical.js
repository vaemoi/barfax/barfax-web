const
  fs = require(`fs`),
  cheerio = require(`cheerio`),
  path = require(`path`);


const indexHTML = cheerio.load(fs.readFileSync(
  path.join(__dirname, `../public/index.html`)
));

let criticalCSS = fs.readFileSync(
  path.join(__dirname, `../public/css/critical.css`)
);

if (Buffer.isBuffer(criticalCSS)) {
  criticalCSS = criticalCSS.toString();
}

indexHTML(`<style>${criticalCSS}</style>`).insertAfter(`title`);

fs.writeFileSync(path.join(__dirname, `../public/index.html`), indexHTML.html());

const
  cheerio = require(`cheerio`),
  crossFetch = require(`cross-fetch`), // TODO maybe: replace with 'http'
  fs = require(`fs`),
  path = require(`path`),
  semver = require(`semver`);

class OrpinError extends Error {
  constructor(message) {
    super(message);
    this.name = this.constructor.name;
    this.stack = Error.captureStackTrace(this, this.constructor);
  }
}

class ValidationError extends OrpinError {}

class MissingConfigError extends ValidationError {
  constructor(key) {
    super(`Missing ${key} in orpin config`);
  }
}

class MissingConfigValueError extends MissingConfigError {
  constructor(key) {
    super(`value for ${key}`);
  }
}

class MissingPackagePathError extends MissingConfigError {
  constructor(packageName) {
    super(`path or devPath for ${packageName}`);
  }
}

class ReadError extends ValidationError {
  constructor(fileName, filePath) {
    super(`Path to ${fileName} -- doesn't exist\n\t${filePath}\n`);
  }
}

class ServerResponseError extends ValidationError {
  constructor(code, url) {
    let message;

    switch(code) {
    case code < 500:
      message = `Problem with request -- probably a bad url`; break;
    case code < 600: message = ``;
      message = `Problem with request -- server error`; break;
    }

    super(`${message}\n\t${url}\n`);
  }
}

const orpinAtom = (configPath) => {
  if (!fs.existsSync(configPath)) {
    throw new ReadError(`.orpinrc`, configPath);
  }

  const
    basePath = path.resolve(path.dirname(configPath)),
    config = JSON.parse(fs.readFileSync(path.join(basePath, configPath))),
    packageJSONPath = path.join(basePath, `package.json`);

  if (!fs.existsSync(packageJSONPath)) {
    throw new ReadError(`package.json`, packageJSONPath);
  }

  if (!config.indexFile) {
    throw new MissingConfigValueError(`indexFile`);
  }

  const indexPath = path.join(basePath, config.indexFile);

  if (!fs.existsSync(indexPath)) {
    throw new ReadError(path.basename(indexPath), indexPath);
  }

  if (!config.vendorPath) {
    throw new MissingConfigValueError(`vendorPath`);
  }

  const vendorPath = path.join(basePath, config.vendorPath);

  if (!fs.existsSync(vendorPath)) {
    fs.mkdirSync(vendorPath, {recursive: true});
  }

  return {
    packageJSONPath: packageJSONPath,
    initial: config,
    indexPath: indexPath,
    vendorPath: vendorPath
  };
};

/**
 * Compile all package info into one place
 *
 * @param      {Object}  buildInfo        meta data for each package
 * @param      {Object}  packageVersions  Package version numbers
 *
 * @return     {Object}  package infos
 */
const orpinBuildPkgs = (buildInfo, packageVersions) => {
  return Object.entries(packageVersions).reduce((final, pkg) => {
    const pkgName = pkg[0];

    if (buildInfo.packages[pkgName] && !buildInfo.packages[pkgName].skip) {
      const pkgInfo = buildInfo.packages[pkgName];

      let pkgPath = pkgInfo.path;

      if (!process.env.PRODUCTION && pkgInfo.devPath) {
        pkgPath = pkgInfo.devPath;
      }

      if (!pkgPath) {
        throw new MissingPackagePathError(pkgName);
      }

      const pkgVersion = semver.valid(semver.coerce(pkg[1]));

      pkgPath = `${pkgName}@${pkgVersion}/${pkgPath}`;

      final[pkgName] = {
        name: pkgName,
        version: pkgVersion,
        path: `${pkgPath}`,
        cdnURI: `${buildInfo.baseURL}/npm/${pkgPath}`,
        localURI: `${buildInfo.vendorSrcPrefix}/${pkgPath}`
      };
    }

    return final;
  }, {});
};

/**
 * Bootstrap orpin
 *
 * @param      {String}  [maybeConfigPath=``]  Where the configuration file is
 *
 * @return     {Object}  Necessary data to run orpin
 *
 */
const orpinBootstrap = (maybeConfigPath) => {
  const
    configPath = maybeConfigPath ? maybeConfigPath : `./.orpinrc`,
    atom = orpinAtom(configPath),
    packageJSON = require(atom.packageJSONPath),
    packages = orpinBuildPkgs(atom.initial, packageJSON.dependencies);

  return {
    anchor: atom.initial.anchorSelector,
    indexPath: atom.indexPath,
    vendorPath: atom.vendorPath,
    pkgs: packages
  };
};

/**
 * Fetch files from cdn and save them locally
 *
 * @param      {Object}  info  package data needed to construct the urls & paths
 * @param      {String}  writePath    Where to write the file
 *
 */
const orpinFetch = async (info, writePath) => {
  let response = await crossFetch(info.cdnURI);

  if (!response.ok) {
    throw new ServerResponseError(info.cdnURI, response.status);
  }

  response = await response.text();

  if (response === null || response.length === 0) {
    console.log(`(${info.name}) Fetched empty file`);
  }

  const uri = `${writePath}/${info.path}`;

  if (!fs.existsSync(path.dirname(uri))) {
    fs.mkdirSync(path.dirname(uri), {recursive: true});
  }

  fs.writeFileSync(uri, response);
};

/**
 * Inject a script tag into an HTML DOM tree
 *
 * @param      {String}    anchor      Selector for the element that will come after the inejcted tag
 * @param      {Function}  domWrapper  A cheerio object representing the DOM for indexFile
 * @param      {String}    srcURI      The value for the 'src=' attribute on the injected script tag
 *
 */
const orpinInject = (anchor, domWrapper, srcURI) => {
  domWrapper(`script[src='${srcURI}']`).remove();

  if (process.env.PRODUCTION) {
    // TODO: Add flag for combining into single url
    domWrapper(`<srcipt crossorigin defer src="${srcURI}"></script>`).insertBefore(anchor);
  } else {
    domWrapper(`<srcipt defer src="${srcURI}"></script>`).insertBefore(anchor);
  }
};

/**
 * Warp your npm dependencies from the cdn into your local dev and index file
 *    for the browser
 *
 */
const orpin = async (configPath) => {
  try {
    console.log(`Starting!`);
    const core = orpinBootstrap(configPath);

    const indexHTML = cheerio.load(fs.readFileSync(core.indexPath));
    const originalHTML = indexHTML.html();

    console.log(`Fetching & Injecting`);
    for (const pkgName of Object.keys(core.pkgs)) {
      const pkg = core.pkgs[pkgName];

      // Download files for offline use in development
      if (!process.env.PRODUCTION) {
        await orpinFetch(pkg, core.vendorPath);
      }

      // Inject files to index file
      orpinInject(
        core.anchor, indexHTML, process.env.PRODUCTION ? pkg.cdnURI : pkg.localURI
      );
    }

    const finalHTML = indexHTML.html();

    if (originalHTML !== finalHTML) {
      fs.writeFileSync(core.indexPath, finalHTML);
      console.log(`Finished!`);
    }
  } catch (err) {
    console.log(err);
  }
};

// TODO: take commandline argument for config path
orpin(null, process.env.PRODUCTION);
